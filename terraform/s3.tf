# TODO : Create a s3 bucket with aws_s3_bucket

resource "aws_s3_bucket" "s3_job_offer_bucket_shy" {
  bucket = "s3-job-offer-bucket-shy"
  acl    = "public-read"
  force_destroy = true
}


# TODO : Create 1 nested folder :  job_offers/raw/  |  with  aws_s3_bucket_object

resource "aws_s3_bucket_object" "object" {
  bucket = aws_s3_bucket.s3_job_offer_bucket_shy.id
  key    = "job_offers/raw/"
  source = "/dev/null"

  # The filemd5() function is available in Terraform 0.11.12 and later
  # For Terraform 0.11.11 and earlier, use the md5() function and the file() function:
  # etag = "${md5(file("path/to/file"))}"
 # etag = filemd5("job_offers/raw/")
}

# TODO : Create an event to trigger the lambda when a file is uploaded into s3 with aws_s3_bucket_notification

resource "aws_s3_bucket_notification" "bucket_notification" {
  bucket = aws_s3_bucket.s3_job_offer_bucket_shy.id

  lambda_function {
    lambda_function_arn = aws_lambda_function.tp2_lambda.arn
    events              = ["s3:ObjectCreated:*"]
    filter_prefix       = "job_offers/raw/"
    filter_suffix       = ".csv"
  }

  depends_on = [aws_lambda_permission.allow_bucket]
}